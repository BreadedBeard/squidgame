var inst = argument0;
var x_bound = argument1;
var y_bound = argument2;
var x_dir = argument3;
var y_dir = argument4;

var x_check = scr_threshold_check(inst.bbox_left, inst.bbox_right ,x_bound, x_dir<0);
var y_check = scr_threshold_check(inst.bbox_top, inst.bbox_bottom ,y_bound, y_dir<0);

if x_check and y_check  return true;
else                    return false;
