inst_x = argument0.x;
inst_y = argument0.y;

var bottom_slope = instance_place(inst_x ,inst_y+1, obj_slope_parent);
if bottom_slope == noone{
    var right_slope = instance_place(inst_x+1 ,inst_y, obj_slope_parent);
    if right_slope == noone return instance_place(inst_x-1 ,inst_y, obj_slope_parent);
    else return right_slope;
}else   return bottom_slope;
